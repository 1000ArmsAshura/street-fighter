import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) { 
  document.body.addEventListener("keydown", function(e){
    if(e.key == controls.PlayerOneAttack){
      secondFighter.health -= getDamage(firstFighter, secondFighter)
    }
  })

  return new Promise((resolve) => {
    if (firstFighter.health == 0) {
      resolve(secondFighter)
    } else if (secondFighter.health == 0) {
      resolve(firstFighter)
    }
  });
}

export function getDamage(attacker, defender) {
  let attackerDamage = getHitPower(attacker)
  let defenderBlock = getBlockPower(defender)

  if (defenderBlock > attackerDamage) {
    return 0
  } else {
    return attackerDamage - defenderBlock
  }
}

export function getHitPower(fighter) {
  return fighter.attack * Math.random() + 1;
}

export function getBlockPower(fighter) {
  return fighter.defense * Math.random() + 1;
}
